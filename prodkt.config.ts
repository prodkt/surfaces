import { createProdkt } from '@prodkt/ramp-core';

export const { css, keyframes } = createProdkt({
  theme: {
    colors: {
      white: '#fff',
      gray100: '#ccc',
      gray300: '#aaa',
      black: '#111',
      red: 'crimson',
      green: 'green',
    },
  },
});
