# `number`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/number
# or
$ npm install @prodkt/number
```

## Usage

This is an internal utility, not intended for public usage.
