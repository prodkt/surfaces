# `rect`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/rect
# or
$ npm install @prodkt/rect
```

## Usage

This is an internal utility, not intended for public usage.
