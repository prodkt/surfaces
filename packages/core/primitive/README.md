# `primitive`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/primitive
# or
$ npm install @prodkt/primitive
```

## Usage

This is an internal utility, not intended for public usage.
