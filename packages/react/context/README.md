# `react-context`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-context
# or
$ npm install @prodkt/react-context
```

## Usage

This is an internal utility, not intended for public usage.
