# `react-tabs`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-tabs
# or
$ npm install @prodkt/react-tabs
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/components/tabs).
