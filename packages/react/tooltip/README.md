# `react-tooltip`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-tooltip
# or
$ npm install @prodkt/react-tooltip
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/components/tooltip).
