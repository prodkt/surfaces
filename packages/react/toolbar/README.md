# `react-toolbar`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-toolbar
# or
$ npm install @prodkt/react-toolbar
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/components/toolbar).
