# `react-popper`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-popper
# or
$ npm install @prodkt/react-popper
```

## Usage

This is an internal utility, not intended for public usage.
