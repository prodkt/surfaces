# `react-toggle-group`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-toggle-group
# or
$ npm install @prodkt/react-toggle-group
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/components/toggle-group).
