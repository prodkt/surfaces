# `react-separator`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-separator
# or
$ npm install @prodkt/react-separator
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/components/separator).
