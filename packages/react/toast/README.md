# `react-toast`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-toast
# or
$ npm install @prodkt/react-toast
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/components/toast).
