# `react-roving-focus`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-roving-focus
# or
$ npm install @prodkt/react-roving-focus
```

## Usage

This is an internal utility, not intended for public usage.
