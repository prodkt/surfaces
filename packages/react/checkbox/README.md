# `react-checkbox`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-checkbox
# or
$ npm install @prodkt/react-checkbox
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/components/checkbox).
