# `react-menu`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-menu
# or
$ npm install @prodkt/react-menu
```

## Usage

This is an internal utility, not intended for public usage.
