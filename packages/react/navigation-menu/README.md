# `react-navigation-menu`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-navigation-menu
# or
$ npm install @prodkt/react-navigation-menu
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/components/navigation-menu).
