# `react-dropdown-menu`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-dropdown-menu
# or
$ npm install @prodkt/react-dropdown-menu
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/components/dropdown-menu).
