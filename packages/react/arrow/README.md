# `react-arrow`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-arrow
# or
$ npm install @prodkt/react-arrow
```

## Usage

This is an internal utility, not intended for public usage.
