# `react-menubar`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-menubar
# or
$ npm install @prodkt/react-menubar
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/components/menubar).
