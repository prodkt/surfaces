# `react-dialog`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-dialog
# or
$ npm install @prodkt/react-dialog
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/components/dialog).
