# `react-label`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-label
# or
$ npm install @prodkt/react-label
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/utilities/label).
