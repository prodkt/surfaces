# `react-accordion`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-accordion
# or
$ npm install @prodkt/react-accordion
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/components/accordion).
