# `react-radio-group`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-radio-group
# or
$ npm install @prodkt/react-radio-group
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/components/radio-group).
