# `react-collection`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-collection
# or
$ npm install @prodkt/react-collection
```

## Usage

This is an internal utility, not intended for public usage.
