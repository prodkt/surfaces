# `react-announce`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-announce
# or
$ npm install @prodkt/react-announce
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/utilities/announce).
