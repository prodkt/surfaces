# `react-slot`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-slot
# or
$ npm install @prodkt/react-slot
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/utilities/slot).
