# `react-presence`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-presence
# or
$ npm install @prodkt/react-presence
```

## Usage

This is an internal utility, not intended for public usage.
