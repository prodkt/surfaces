# `react-collapsible`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-collapsible
# or
$ npm install @prodkt/react-collapsible
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/components/collapsible).
