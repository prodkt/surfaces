# `react-avatar`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-avatar
# or
$ npm install @prodkt/react-avatar
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/components/avatar).
