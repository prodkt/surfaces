# `react-aspect-ratio`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-aspect-ratio
# or
$ npm install @prodkt/react-aspect-ratio
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/utilities/aspect-ratio).
