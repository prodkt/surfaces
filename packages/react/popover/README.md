# `react-popover`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-popover
# or
$ npm install @prodkt/react-popover
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/components/popover).
