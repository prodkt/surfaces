# `react-direction`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-direction
# or
$ npm install @prodkt/react-direction
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/utilities/direction).
