# `react-toggle`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-toggle
# or
$ npm install @prodkt/react-toggle
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/components/toggle).
