# `react-form`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-form
# or
$ npm install @prodkt/react-form
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/components/form).
