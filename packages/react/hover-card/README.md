# `react-hover-card`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-hover-card
# or
$ npm install @prodkt/react-hover-card
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/components/hover-card).
