# `react-alert-dialog`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-alert-dialog
# or
$ npm install @prodkt/react-alert-dialog
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/components/alert-dialog).
