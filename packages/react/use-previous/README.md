# `react-use-previous`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-use-previous
# or
$ npm install @prodkt/react-use-previous
```

## Usage

This is an internal utility, not intended for public usage.
