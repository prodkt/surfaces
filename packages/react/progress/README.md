# `react-progress`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-progress
# or
$ npm install @prodkt/react-progress
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/components/progress).
