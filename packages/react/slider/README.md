# `react-slider`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-slider
# or
$ npm install @prodkt/react-slider
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/components/slider).
