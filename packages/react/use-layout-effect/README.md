# `react-use-layout-effect`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-use-layout-effect
# or
$ npm install @prodkt/react-use-layout-effect
```

## Usage

This is an internal utility, not intended for public usage.
