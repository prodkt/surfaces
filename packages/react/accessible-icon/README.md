# `react-accessible-icon`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-accessible-icon
# or
$ npm install @prodkt/react-accessible-icon
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/utilities/accessible-icon).
