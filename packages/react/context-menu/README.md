# `react-context-menu`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-context-menu
# or
$ npm install @prodkt/react-context-menu
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/components/context-menu).
