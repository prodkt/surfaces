# `react-id`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-id
# or
$ npm install @prodkt/react-id
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/utilities/id-provider).
