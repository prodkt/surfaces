# `react-use-rect`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-use-rect
# or
$ npm install @prodkt/react-use-rect
```

## Usage

This is an internal utility, not intended for public usage.
