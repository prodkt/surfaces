# `react-focus-guards`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-focus-guards
# or
$ npm install @prodkt/react-focus-guards
```

## Usage

This is an internal utility, not intended for public usage.
