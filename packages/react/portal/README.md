# `react-portal`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-portal
# or
$ npm install @prodkt/react-portal
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/utilities/portal).
