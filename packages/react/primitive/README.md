# `react-primitive`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-primitive
# or
$ npm install @prodkt/react-primitive
```

## Usage

This is an internal utility, not intended for public usage.
