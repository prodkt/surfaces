# `react-select`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-select
# or
$ npm install @prodkt/react-select
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/components/select).
