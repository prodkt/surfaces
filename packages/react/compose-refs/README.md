# `react-compose-refs`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-compose-refs
# or
$ npm install @prodkt/react-compose-refs
```

## Usage

This is an internal utility, not intended for public usage.
