# `react-focus-scope`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-focus-scope
# or
$ npm install @prodkt/react-focus-scope
```

## Usage

This is an internal utility, not intended for public usage.
