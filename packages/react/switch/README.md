# `react-switch`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-switch
# or
$ npm install @prodkt/react-switch
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/components/switch).
