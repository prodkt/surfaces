# `react-scroll-area`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-scroll-area
# or
$ npm install @prodkt/react-scroll-area
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/components/scrollbar).
