# `react-dismissable-layer`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-dismissable-layer
# or
$ npm install @prodkt/react-dismissable-layer
```

## Usage

This is an internal utility, not intended for public usage.
