# `react-use-controllable-state`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-use-controllable-state
# or
$ npm install @prodkt/react-use-controllable-state
```

## Usage

This is an internal utility, not intended for public usage.
