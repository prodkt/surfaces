# `react-use-callback-ref`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-use-callback-ref
# or
$ npm install @prodkt/react-use-callback-ref
```

## Usage

This is an internal utility, not intended for public usage.
