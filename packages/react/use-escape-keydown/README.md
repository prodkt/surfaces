# `react-use-escape-keydown`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-use-escape-keydown
# or
$ npm install @prodkt/react-use-escape-keydown
```

## Usage

This is an internal utility, not intended for public usage.
