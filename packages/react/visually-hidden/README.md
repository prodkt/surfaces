# `react-visually-hidden`

## Installation

```sh
echo @prodkt:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
# then
$ pnpm add @prodkt/react-visually-hidden
# or
$ npm install @prodkt/react-visually-hidden
```

## Usage

View docs [here](https://prodkt.cloud/surfaces/docs/utilities/visually-hidden).
