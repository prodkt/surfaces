[![Prodkt Surfaces Logo](og_surfaces.jpg)](https://prodkt.cloud/surfaces)

# Prodkt Surfaces

You may run into issues regarding packages not self registering. Make sure you're running the workspace version of Typescript not VSCode's version of Typescript and (Windows: ctrl + shift + p) or (Mac: cmd + shift + p) > reload Window: developer should bring your build back to healthy.

I use Turbo Repo, installed globally and I use it in nearly all of my monorepo projects. Double check you've installed Turbo globally as well.
